#!/bin/bash

DIR=$(pwd)
REMOTE="git@bitbucket.org:cwcrawley/bootstrap.git"
LOCAL="/Users/cwcrawley/sites/bootstrap"

#                     .^.,*.
#                    (   )  )
#                   .~       "-._   _.-'-*'-*'-*'-*'-'-.--._
#                 /'             `"'                        `.
#               _/'                                           `.
#          __,""                                                ).--.
#       .-'       `._.'                                          .--.\
#      '                                                         )   \`:
#     ;                            MADE BY HIPPO                 ;    "
#    :                                                           )
#    | 8                                                        ;
#     =                  )                                     .
#      \                .                                    .'
#       `.            ~  \                                .-'
#         `-._ _ _ . '    `.          ._        _        |
#                           |        /  `"-*--*' |       |  
#                           |        |           |       :
# ~~~~~~~---   ~-~-~-~   -~-~-~-~-~-~~~~~~  ~~~~  ~-~-~-~-~-~-~-
#------~~~~~~~~~----------~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
# ~~~~~~~~~   ~~~~~~~~~       ~~~~~~~   ~~~~~~~~~  ~~~~~~~~~~~~~~~

function func_perms
{
			CHMOD 666 $SYSDIR/expressionengine/config/config.php
			CHMOD 666 $SYSDIR/expressionengine/config/database.php
			
			CHMOD 777 $SYSDIR/expressionengine/cache/
			
			CHMOD 777 images/avatars/uploads/
			CHMOD 777 images/captchas/
			CHMOD 777 images/member_photos/
			CHMOD 777 images/pm_attachments/
			CHMOD 777 images/signature_attachments/
			CHMOD 777 images/uploads/
			
			CHMOD 777 assets/templates/
			
			CHMOD -R 755 assets/ugc/
			
			echo 'done'
}

function func_cp
{
			sed "s/\$system_path = '\.\/$SYSDIR'\;/\$system_path = '\.\/$NEWSYS'\;/g" index.php > tmp
			mv tmp index.php 
			
			sed "s/\$system_path = '\.\/$SYSDIR'\;/\$system_path = '\.\/$NEWSYS'\;/g" admin.php > tmp
			mv tmp admin.php 
			
			sed "s/\$system_folder = \"$SYSDIR\"\;/\$system_folder = \"$NEWSYS\"\;/g" assets/config/config.php > assets/config/tmp
			mv assets/config/tmp assets/config/config.php 
			
			mv $SSYDIR $NEWSYS
			
			if [ "$REM" == "yes" ]; then
				rm -f admin.php
			fi
			
			echo 'CP Folder Reset'	
}

function func_clean
{			
			rm -Rf $SYSDIR/installer
			rm -Rf themes/site_themes/agile_records
			rm -Rf themes/wiki_themes/
			rm -Rf themes/cp_themes/corporate/
			rm -Rf themes/profile_themes/agile_records/
			
			echo 'done.'
}

function func_backup_db
{
			EPOCH=$(date +%s)
			
			mysqldump -u$DBUSER -p$DBPASS $DBNAME > assets/backups/${DBNAME}_${EPOCH}.sql
			
			echo 'done.'
			
}

function func_setup_site
{
			cd $DIR
			
			/bin/rm -Rf .*
			/bin/rm -Rf *
			#/usr/local/git/bin/git clone $REMOTE $DIR
			/usr/local/git/bin/git clone $LOCAL $DIR
			/bin/rm -Rf .git
			
			echo 'done.'
}

function func_create_db
{

	mysql -u$DBUSER -p$DBPASS -e "CREATE DATABASE $DBNAME;"
	
	echo 'done.'

}


function func_setup_db
{
	cd assets/backups/
	
	FNAME=`ls -t |tail -1`
	
	mysql -u$DBUSER -p$DBPASS $DBNAME < $FNAME
	
	cd ../config
	
	sed "s/$db\['local'\]\['database'\] =.*/$db\['local'\]\['database'\] = '$DBNAME'\;/g" database.php > tmp
	mv tmp database.php
	cd ../..
	
	echo 'done.'
	
}

function func_create_repo
{
	echo 'initialising local GIT repo'
	git init
	
	echo 'adding all files'
	git add *
	
	echo 'committing all files'
	git commit -m "Initial Commit via Bootstrap"
	
	echo 'adding remote repo'
	git remote add origin $NEW_REPO
	
	echo 'pushing files'
	git push origin master
	
	echo 'done.'
	
}


while getopts ":bscpa" Option
do
	case $Option in

		a )
			
			SYSDIR=$2
			NEWSYS=$3
			REM=$4
			
			#func_perms
			func_cp
		
		;;
		
		b )
			
			DBNAME=$2
			DBUSER=$3
			DBPASS=$4
			
			echo "######### BACKING UP DATABASE ########"
			
			func_backup_db
		
		;;

		s )
		
			DBNAME=$2
			DBUSER=$3
			DBPASS=$4
			DBCREATE=$5
			NEW_REPO=$6
			
			echo "######### PULLING FROM REMOTE GIT REPO #########"
			func_setup_site
			
			if [ "$5" == "TRUE" ]
			then
				echo "######### CREATING NEW DATABASE #########"
				func_create_db
			fi
			
			if [ ! -z "$2" ]
			then
				echo "######### IMPORTING REMOTE DATABASE #########"
				func_setup_db
			fi
			
			if [ ! -z "$6" ]
			then
				echo "######### INITIAL COMMIT ########"
				func_create_repo
			fi		
			
		;;
		
		p )
			SYSDIR=$2
			
			echo "######### UPDATING PERMISSIONS ########"
			
			func_perms
		;;
		
		c )
			echo "######### CLEANING INSTALL ########"
			
			func_clean
		;;
		
		* )
			echo "Unimplemented option chosen"
		;;
	esac
done

shift $(($OPTIND - 1))