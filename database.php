<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

switch($_SERVER['REMOTE_ADDR']) {
	case '127.0.0.1':
	$active_group = 'local';
	break;
	
	case '123.456.789.000':
	$active_group = 'test';
	break;
	
	default:
	$active_group = 'live';
	break;	
}

$active_record = TRUE;

$db['local']['hostname'] = "localhost";
$db['local']['username'] = "root";
$db['local']['password'] = "root";
$db['local']['database'] = "bootstrap";
$db['local']['dbdriver'] = "mysql";
$db['local']['dbprefix'] = "exp_";
$db['local']['pconnect'] = FALSE;
$db['local']['swap_pre'] = "exp_";
$db['local']['db_debug'] = TRUE;
$db['local']['cache_on'] = FALSE;
$db['local']['autoinit'] = FALSE;
$db['local']['char_set'] = "utf8";
$db['local']['dbcollat'] = "utf8_general_ci";
$db['local']['cachedir'] = $_SERVER['DOCUMENT_ROOT']."/system/expressionengine/cache/db_cache/";


$db['test']['hostname'] = "localhost";
$db['test']['username'] = "user";
$db['test']['password'] = "pass";
$db['test']['database'] = "dbname";
$db['test']['dbdriver'] = "mysql";
$db['test']['dbprefix'] = "exp_";
$db['test']['pconnect'] = FALSE;
$db['test']['swap_pre'] = "exp_";
$db['test']['db_debug'] = TRUE;
$db['test']['cache_on'] = FALSE;
$db['test']['autoinit'] = FALSE;
$db['test']['char_set'] = "utf8";
$db['test']['dbcollat'] = "utf8_general_ci";
$db['test']['cachedir'] = $_SERVER['DOCUMENT_ROOT']."/system/expressionengine/cache/db_cache/";


$db['live']['hostname'] = "localhost";
$db['live']['username'] = "user";
$db['live']['password'] = "pass";
$db['live']['database'] = "dbname";
$db['live']['dbdriver'] = "mysql";
$db['live']['dbprefix'] = "exp_";
$db['live']['pconnect'] = FALSE;
$db['live']['swap_pre'] = "exp_";
$db['live']['db_debug'] = TRUE;
$db['live']['cache_on'] = FALSE;
$db['live']['autoinit'] = FALSE;
$db['live']['char_set'] = "utf8";
$db['live']['dbcollat'] = "utf8_general_ci";
$db['live']['cachedir'] = $_SERVER['DOCUMENT_ROOT']."/system/expressionengine/cache/db_cache/";

/* End of file database.php */